package com.company.myapp.controller;


import static org.springframework.web.bind.annotation.RequestMethod.GET;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/")
public class HomeController {
	static final Logger logger = Logger.getLogger(HomeController.class);
	String className = this.getClass().getSimpleName();
	String msg = "";

	@RequestMapping(path="default",method=GET)
	public String defaultPage(ModelMap model) {
		msg = className +  "/defaultPage";
		model.addAttribute("message",msg);
		return "default";
	}
	
	@RequestMapping(path="home*",method=GET)
	public String homePage(ModelMap model) {
		msg = className +  "/homePage";
		model.addAttribute("message",msg);
		return "home";
	}
}
