package com.company.myapp.controller;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

//import com.company.myapp.service.Observer;

@Controller
public class ObserverController {
	private static final Logger logger = Logger.getLogger(ObserverController.class);
	
//	@Autowired
//	private Subject subject;
	
//	@Autowired
//	@Qualifier("observerRemote")
//	private Observer observer;
	
	@RequestMapping(value = "/newObserver", method = GET)
	@ResponseBody
	public String newObserver() {
		logger.info("ObserverController/newObserver");

//		Observer observer = new ObserverRemote();
//		observer.subscribe(subject);
//		String id = observer.getId();

		return "A new Observer ID:"
//		+ id + " has been subscribed"
				;
	}

}