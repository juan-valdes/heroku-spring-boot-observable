package com.company.myapp.controller;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.company.myapp.data.FeedObserversRepository;

@Controller
public class FeedController {

	private static final Logger logger = Logger.getLogger(FeedController.class);
	String className = this.getClass().getSimpleName();
	String msg = "";
	
	@Autowired
	private FeedObserversRepository feedObserversRepository;
	
	@RequestMapping(value = "/lastFeed", method = GET)
	public String lastFeed(Model model) {
		logger.info("FeedController/lastFeed");
		
		msg = className +  "/lastFeed";
		model.addAttribute("message",msg);

//		Feed newestFeed = feedObserversRepository.getNewestFeed();
		
//		if (newestFeed != null) {
//			logger.info("newestFeed: " + newestFeed.toString());
//			model.addAttribute("lastFeed", newestFeed);
//		}
		return "last-feed";
	}
}
