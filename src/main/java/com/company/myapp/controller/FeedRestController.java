package com.company.myapp.controller;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.company.myapp.model.Feed;

@Controller
public class FeedRestController {
	
	private static final Logger logger = Logger.getLogger(FeedRestController.class);
	
//	@Autowired
//	private FeedRepository feedRepository; 
	
	@RequestMapping(value = "/lastFeedRest", produces = MediaType.APPLICATION_JSON_VALUE, method = GET)
	@ResponseBody
	public Feed lastFeed() {
		logger.info("FeedRestController/lastFeed");

//		Feed newestFeed = feedRepository.getNewestFeed();
//		if (newestFeed == null) {
//			logger.debug("feed is NULL");
			return new Feed();
//		} else {
//			logger.info("Last Feed: " + newestFeed.toString());
//			return newestFeed;
//		}
	}
	
	@RequestMapping(value = "/lastFeedRest", produces = MediaType.APPLICATION_JSON_VALUE, method = POST)
	@ResponseBody
	public Feed lastFeedPost(@RequestParam(value="observer-id") String observerIdStr) {
		logger.info("FeedRestController/lastFeedRest");
		
//		int observerId = Integer.valueOf(observerIdStr);
//		if(observerId > 999 && observerId < 2001){
//			Feed newestFeed = feedRepository.getNewestFeed();
//			if(newestFeed == null){
//				logger.debug("feed is NULL");
//				return new Feed();
//			} else {
//				logger.info("newestFeed(observer-id=" + observerId +  "): " + newestFeed.toString());
//				return newestFeed;
//			}
//		} else {
//			logger.info("Observer Id not recognized");
			return new Feed();
//		}
	}
}
