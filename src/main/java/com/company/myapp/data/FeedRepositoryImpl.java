package com.company.myapp.data;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import com.company.myapp.model.Feed;
import com.company.myapp.service.UUIDGenerator;

@Component
public class FeedRepositoryImpl implements FeedRepository {
	private static final Logger logger = Logger.getLogger(FeedRepositoryImpl.class);
	
	private static final int OLDEST_INDEX = 0;
	private static final int ID_LENGHT = 6;
	private final String ID = UUIDGenerator.nextUUID(ID_LENGHT);
	
	private List<Feed> feedList = new ArrayList<Feed>();

	@Override
	public String getId() {
		return ID;
	}
	
	@Override
	public void addNewestFeed(Feed lastFeed) {
		if(lastFeed.getEntryId() > getNewestId()){
			feedList.add(lastFeed);
		}
	}
	
	@Override
	public Feed getNewestFeed() {
		if(feedList.size() > 0){
			return feedList.get(feedList.size()-1);
		}
		return null;
	}

	@Override
	public void removeOldestFeed() {
		if(feedList.size() > 1){
			feedList.remove(OLDEST_INDEX);
		}
	}
	
	private Long getNewestId(){
		if(feedList.size() > 0){
			return feedList.get(feedList.size()-1).getEntryId();
		}
		return 0L;
	}
}
