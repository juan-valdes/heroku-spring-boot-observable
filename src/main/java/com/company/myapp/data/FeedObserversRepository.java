package com.company.myapp.data;

import java.util.List;

import com.company.myapp.model.Feed;
import com.company.myapp.model.FeedObservers;
import com.company.myapp.service.Bus;

public interface FeedObserversRepository {
	public String getId();

	public String getOwnerId();

	public void setOwnerId(String ownerId);

	void subscribeToNotifier();
	
	void removeFromNotifier();
	
	public List<Bus> getObserverList();

	public void setObserverList(List<Bus> observerList);

	public void addObserver(Bus observer);

	public void removeObserver(Bus observer);

	public List<FeedObservers> getFeedObserversList();

	public void addNewFeed(Feed newFeed);

	public void displayFeedObserversList();
}
