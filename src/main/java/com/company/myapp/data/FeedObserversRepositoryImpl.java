package com.company.myapp.data;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.company.myapp.model.Feed;
import com.company.myapp.model.FeedObservers;
import com.company.myapp.service.Bus;
import com.company.myapp.service.FeedNotifier;
import com.company.myapp.service.UUIDGenerator;

//@Component
public class FeedObserversRepositoryImpl implements FeedObserversRepository {
	private static final Logger logger = Logger.getLogger(FeedObserversRepositoryImpl.class);

	private static final int ID_LENGHT = 6;
	private static final int OLDEST_INDEX = 0;
	
//	@Value("${maxFeeds}")
//	private static int maxFeeds;
	private static final int MAX_FEEDS = 10;
	private static final boolean IGNORE_EMPTY_OBSERVERS_LIST = true;

	private String id = UUIDGenerator.nextUUID(ID_LENGHT);
	private String ownerId;
	private List<Bus> observerList = new ArrayList<Bus>();
	private List<FeedObservers> feedObserversList = new ArrayList<FeedObservers>();
	private FeedNotifier feedNotifier;

	
//	@Autowired
//	public FeedObserversRepositoryImpl(FeedNotifier feedNotifier) {
//		logger.info(" | | MAX_FEEDS: " +  MAX_FEEDS);
//		logger.info("          FO Repo(" + id + ") created");
//		this.feedNotifier = feedNotifier;
//		subscribeToNotifier();
//	}
	
	@Override
	public String getId() {
		return id;
	}
	
	@Override
	public String getOwnerId() {
		return ownerId;
	}

	@Override
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
		logger.info("          FO Repo(" + id + "/" + ownerId +") owner set");
	}
	
	@Override
	public void subscribeToNotifier(){
		feedNotifier.subscribeRepository(this);
		
	}
	
	@Override
	public void removeFromNotifier(){
		feedNotifier.removeRepository(this);
		
	}

	@Override
	public List<Bus> getObserverList() {
		return observerList;
	}

	@Override
	public void setObserverList(List<Bus> observerList) {
		this.observerList = observerList;
	}

	@Override
	public void addObserver(Bus observer) {
		if(observerList.contains(observer)){
			logger.info("         Observer(" + observer.getId() + ") ALREADY subscrived to  FO Repo(" + id + "/" + ownerId + ")");
		} else {
			if(observerList.add(observer)){
				logger.info("         Observer(" + observer.getId() + ")         subscrived to  FO Repo(" + id + "/" + ownerId + ")");
			} else {
				logger.info("         Observer(" + observer.getId() + ")     NOT subscrived to  FO Repo(" + id + "/" + ownerId + ")");
			}
		}
	}

	@Override
	public void removeObserver(Bus observer) {
		if(observerList.remove(observer)){
			logger.info("         Observer(" + observer.getId() + ")          removed from  FO Repo(" + id + "/" + ownerId + ")");
		} else {
			logger.info("         Observer(" + observer.getId() + ")     not subscrived to  FO Repo(" + id + "/" + ownerId + ")");
		}
	}

	@Override
	public List<FeedObservers> getFeedObserversList() {
		return feedObserversList;
	}

	@Override
	public void addNewFeed(Feed newFeed) {
		if(observerList.size() > 0 || IGNORE_EMPTY_OBSERVERS_LIST) {
			if(feedObserversList.size() > 0) {
				
				Long newFeedEntryId = newFeed.getEntryId();
				Long lastFeedEntryId = getLastFeed().getEntryId();
				
				if (newFeedEntryId > lastFeedEntryId) {
					
					if (feedObserversList.size() >= MAX_FEEDS) {
						logger.info("Maximum stored feed observers limit (" + MAX_FEEDS + ") reached");
						removeOldestFeed();
					}
					
					FeedObservers newFeedObservers = new FeedObservers();
					newFeedObservers.setFeedAndObserverList(newFeed, observerList);
					feedObserversList.add(newFeedObservers);
				} else {
					logger.info("          FO Repo(" + id + "/" + ownerId + ") discarding new feed, [" + newFeedEntryId + "], already stacked");
				}
			} else {
				FeedObservers newFeedObservers = new FeedObservers();
				newFeedObservers.setFeedAndObserverList(newFeed, observerList);
				feedObserversList.add(newFeedObservers);
			}
		} else {
			logger.info("          FO Repo(" + id + "/" + ownerId + ") no feed stacking due to empty observers list");
		}
		displayFeedObserversList();
	}

	public Feed getLastFeed() {
		if (feedObserversList.size() > 0) {
			return feedObserversList.get(feedObserversList.size() - 1)
					.getFeed();
		}
		return null;
	}

	public void removeOldestFeed() {
		if (feedObserversList.size() > 1) {
			logger.info("          FO Repo(" + id + "/" + ownerId + ") removing oldest feed, [" + feedObserversList.get(OLDEST_INDEX).getFeed().getEntryId() + "]");
			
			feedObserversList.remove(OLDEST_INDEX);
		}
	}

	@Override
	public void displayFeedObserversList() {
		int i = 1;
		logger.info("");
		for (FeedObservers feedObservers : feedObserversList) {
			logger.info("          FO Repo(" + id + "/" + ownerId + ")        [" + i++ + "]: " + feedObservers.toString());
		}
		logger.info("");
	}
	@Override
	public String toString(){
		String str = "FeedObserversRepository {"
				+ "id: " + id
				+ "ownerId: " + ownerId
				+ ", observerList: " + observerList
				+ "feedObserversList size: " + feedObserversList.size();
		return str;
	}
}
