package com.company.myapp.data;

import com.company.myapp.model.Feed;

public interface FeedRepository {
	public String getId();
	public void addNewestFeed(Feed feed);
	public Feed getNewestFeed();
	public void removeOldestFeed();
}
