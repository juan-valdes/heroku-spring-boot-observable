package com.company.myapp.config;

import java.util.Map;

public interface ReaderProperties {
	public void setPropertiesMap(Map<String, String> readerPropertiesMap);
	public Map<String, String> getPropertiesMap();
}
