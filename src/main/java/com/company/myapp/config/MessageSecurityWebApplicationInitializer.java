package com.company.myapp.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

@Configuration
public class MessageSecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {

}