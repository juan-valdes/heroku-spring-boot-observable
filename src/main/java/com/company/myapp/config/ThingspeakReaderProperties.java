package com.company.myapp.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "thingspeak")
@Component
public class ThingspeakReaderProperties implements ReaderProperties {
	
	private String propName1 ;
	private String propValue1;
	private String propName2;
	private String propValue2;
	private String propName3;
	private String propValue3;
	private String propName4;
	private String propValue4;
	
	private Map<String, String> readerPropertiesMap = new HashMap<String, String>();

	public void setPropertiesMap(Map<String, String> readerPropertiesMap){
		this.readerPropertiesMap = readerPropertiesMap;
	}
	
	public Map<String, String> getPropertiesMap(){
		readerPropertiesMap.put(propName1, propValue1);
		readerPropertiesMap.put(propName2, propValue2);
		readerPropertiesMap.put(propName3, propValue3);
		readerPropertiesMap.put(propName4, propValue4);
		
		return this.readerPropertiesMap;
	}
	public String getPropName1() {
		return propName1;
	}
	public void setPropName1(String propName1) {
		this.propName1 = propName1;
	}
	public String getPropValue1() {
		return propValue1;
	}
	public void setPropValue1(String propValue1) {
		this.propValue1 = propValue1;
	}
	public String getPropName2() {
		return propName2;
	}
	public void setPropName2(String propName2) {
		this.propName2 = propName2;
	}
	public String getPropValue2() {
		return propValue2;
	}
	public void setPropValue2(String propValue2) {
		this.propValue2 = propValue2;
	}
	public String getPropName3() {
		return propName3;
	}
	public void setPropName3(String propName3) {
		this.propName3 = propName3;
	}
	public String getPropValue3() {
		return propValue3;
	}
	public void setPropValue3(String propValue3) {
		this.propValue3 = propValue3;
	}
	public String getPropName4() {
		return propName4;
	}
	public void setPropName4(String propName4) {
		this.propName4 = propName4;
	}
	public String getPropValue4() {
		return propValue4;
	}
	public void setPropValue4(String propValue4) {
		this.propValue4 = propValue4;
	}
}