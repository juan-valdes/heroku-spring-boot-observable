package com.company.myapp.service;

import java.util.List;

import com.company.myapp.model.Feed;

public interface Bus {
	
	public String getId();
	
	public Subject getSubject();
	
	public void subscribeTo(Subject subject);

	public void update(Feed feed);

	public void suscribeObserver(Bus observer);

	public void removeObserver(Bus observer);

	public void setFeed(Feed feed);

	public void setObserverList(List<Bus> observerList);

	public List<Bus> getObserverList();

	public void stateChanged(Feed feed);

	public void stackFeed(Feed feed);

}
