package com.company.myapp.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("feedNotifierRunner")
public class FeedNotifierRunner implements Runnable{
	private static final Logger logger = Logger.getLogger(FeedNotifierRunner.class);
	
	@Autowired
	public FeedNotifier feedNotifier;
	
	public void run() {
		feedNotifier.notifyObservers();
	}

}
