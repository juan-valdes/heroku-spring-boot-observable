package com.company.myapp.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("feedReaderRunner")
public class FeedReaderRunner implements Runnable{
	Logger logger = Logger.getLogger(FeedReaderRunner.class);
	
	@Autowired
	@Qualifier("thingspeakFeedReader")
	public FeedReader feedReader;
	
	@Autowired
	public Subject subject;
	
	public void run() {
		subject.setFeed(feedReader.readLastFeed());
	}

}
