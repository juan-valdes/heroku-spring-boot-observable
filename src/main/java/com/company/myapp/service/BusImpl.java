package com.company.myapp.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.company.myapp.model.Feed;

@Component
public class BusImpl implements Bus {
	
	private static final Logger logger = Logger.getLogger(BusImpl.class);
	private static final int ID_LENGHT = 6;
	
	private final String ID = UUIDGenerator.nextUUID(ID_LENGHT);
	private Subject subject;
	private List<Bus> observerList = new ArrayList<Bus>();
//	private FeedObserversRepository feedObserversRepository;

//	@Autowired
//	public BusImpl(FeedObserversRepository feedObserversRepository) {
//		logger.info("              Bus( + ID + ") created");
//		feedObserversRepository.setOwnerId(ID);
//		this.feedObserversRepository = feedObserversRepository;
//	}
	
	@Override
	public String getId() {
		return ID;
	}
	
	@Autowired
	@Override
	public void subscribeTo(Subject subject) {
		logger.info("              Bus(" + ID + ") subcribing to Subject(" + subject.getId() + ")");
		this.subject = subject;
		subject.subscribeBus(this);
	}

	@Override
	public Subject getSubject() {
		return subject;
	}

	@Override
	public void update(Feed feed) {
		logger.info("              Bus(" + ID + ")              update: " + feed);
//		displayFeed(feed);
		setFeed(feed);
	}

	@Override
	public void suscribeObserver(Bus observer) {
		logger.info("              Bus(" + ID + ") NOT IMPLEMENTED subscribing Observer(" + observer.getId() + ")");
//		feedObserversRepository.addObserver(observer);
	}

	@Override
	public void removeObserver(Bus observer) {
		logger.info("              Bus(" + ID + ") NOT IMPLEMENTED removing Observer(" + observer.getId() + ")");
//		feedObserversRepository.removeObserver(observer);
	}

	@Override
	public void setFeed(Feed feed) {
		logger.info("              Bus(" + ID + ")                 set: " + feed);
		stateChanged(feed);
		stackFeed(feed);
	}

	@Override
	public void setObserverList(List<Bus> observerList) {
		this.observerList = observerList;
		logger.info("              Bus(" + ID + ") set observerList (after set): " + this.observerList.toString());
	}

	@Override
	public List<Bus> getObserverList() {
		logger.info("              Bus(" + ID + ") get observerList (actual list): " + observerList.toString());
		return observerList;
	}

	@Override
	public void stateChanged(Feed feed) {
		logger.info("              Bus(" + ID + ") stateChanged");
		stackFeed(feed);
	}

	@Override
	public void stackFeed(Feed feed) {
		logger.info("              Bus(" + ID + ")   stacking NOT IMPL: " + feed);
//		feedObserversRepository.addNewFeed(feed);
	}
}
