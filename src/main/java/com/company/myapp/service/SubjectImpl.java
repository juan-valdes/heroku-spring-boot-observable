package com.company.myapp.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.company.myapp.data.FeedRepository;
import com.company.myapp.model.Feed;

@Component
public class SubjectImpl implements Subject {
	
	private static final Logger logger = Logger.getLogger(SubjectImpl.class);
	private static final int ID_LENGHT = 6;
	
	private final String ID = UUIDGenerator.nextUUID(ID_LENGHT);
	private FeedRepository feedRepository;
	private List<Bus> subscribersList = new ArrayList<>();

	@Autowired
	public SubjectImpl(FeedRepository feedRepository) {
		logger.info("          Subject(" + ID + ") created");
		this.feedRepository = feedRepository;
		logger.info("          Subject(" + ID + ") feedRepository(" + feedRepository.getId() + ") set");
	}
	
	@Override
	public String getId() {
		logger.info("          Subject(" + ID + ") getId: " + ID);
		return ID;
	}

	@Override
	public void setFeed(Feed feed) {
		logger.info("          Subject(" + ID + ")             setFeed: " + feed);
		stateChanged(feed);
	}
	
	public void stateChanged(Feed feed) {
		stackFeed(feed);
		notifyBuses(feed);
		logger.info("          Subject(" + ID + ")        stateChanged: " + feed);
	}
	
	@Override
	public void stackFeed(Feed feed){
		logger.info("          Subject(" + ID + ")           stackFeed: " + feed);
		feedRepository.addNewestFeed(feed);
	}

	@Override
	public boolean subscribeBus(Bus bus) {
		logger.info("          Subject(" + ID + ") subscribeBus: Bus(" + bus.getId() + ")");
		return subscribersList.add(bus);
	}

	@Override
	public boolean removeBus(Bus bus) {
		logger.info("          Subject(" + ID + ") removeBus: Bus(" + bus.getId() + ")");
		return subscribersList.remove(bus);
	}
	
	@Override
	public void notifyBuses(Feed feed){
		logger.info("          Subject(" + ID + ") notifying Buses...");
		for(Bus bus : subscribersList) {
			logger.info("          Subject(" + ID + ") notifying Bus(" + bus.getId() + ")");
			bus.update(feed);
		}
	}
}
