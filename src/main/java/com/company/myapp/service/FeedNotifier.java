package com.company.myapp.service;

import com.company.myapp.data.FeedObserversRepository;

public interface FeedNotifier {
	public String getId();
	public void subscribeRepository(FeedObserversRepository feedObserversRepository);
	public void removeRepository(FeedObserversRepository feedObserversRepository);
	public void notifyObservers();
	public void showFeedObserversRepositoryList();
}
