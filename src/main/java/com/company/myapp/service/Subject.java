package com.company.myapp.service;

import com.company.myapp.model.Feed;

public interface Subject {
	public String getId();
	public boolean subscribeBus(Bus bus);
	public boolean removeBus(Bus bus);
	
	public void setFeed(Feed feed);
	public void stateChanged(Feed feed);
	public void stackFeed(Feed feed);
	public void notifyBuses(Feed feed);
}
