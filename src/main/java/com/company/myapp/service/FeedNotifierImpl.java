package com.company.myapp.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.company.myapp.data.FeedObserversRepository;
import com.company.myapp.model.Feed;
import com.company.myapp.model.FeedObservers;

@Component
public class FeedNotifierImpl implements FeedNotifier {
	private static final Logger logger = Logger.getLogger(FeedNotifierImpl.class);
	private static final int ID_LENGHT = 6;

	private String id = UUIDGenerator.nextUUID(ID_LENGHT);
	
	private List<FeedObserversRepository> feedObserversRepositoryList = new ArrayList<FeedObserversRepository>();
	
	@Override
	public String getId() {
		return id;
	}

	@Override
	public void notifyObservers() {
		
		for (FeedObserversRepository feedObserversRepository : feedObserversRepositoryList) {
			List<FeedObservers> feedObserversList = feedObserversRepository.getFeedObserversList();
			for (FeedObservers feedObservers : feedObserversList) {
				Feed feed = feedObservers.getFeed();
				List<Bus> observersList = feedObservers.getObserverList();
				for (Bus observer : observersList) {
					logger.info("update from FeedNotifier");
					observer.update(feed);
				}
			}
		}
	}
	
	public void subscribeRepository(FeedObserversRepository feedObserversRepository){
		
		if(!feedObserversRepositoryList.contains(feedObserversRepository)){
			feedObserversRepositoryList.add(feedObserversRepository);
			logger.info("          FO Repo(" +  feedObserversRepository.getId() + "/" +  feedObserversRepository.getOwnerId() + ") subscribed to Feed Notifier(" + id + ")");
			showFeedObserversRepositoryList();
		}
	}
	
	public void removeRepository(FeedObserversRepository feedObserversRepository){
		if(feedObserversRepositoryList.remove(feedObserversRepository)){
			logger.info("          FO Repo(" +  feedObserversRepository.getId() + "/" +  feedObserversRepository.getOwnerId() + ") removed from Feed Notifier(" + id + ")");
			showFeedObserversRepositoryList();
		}
	}

	@Override
	public void showFeedObserversRepositoryList() {
		int i = 1;
		for (FeedObserversRepository feedObserversRepo : feedObserversRepositoryList) {
			logger.info("Feed Notifier(" + id + ") FeedObserversRepository [" +  i++ + "]: (" + feedObserversRepo.getId() + "/" + feedObserversRepo.getOwnerId() +")");
		}
	}
}
