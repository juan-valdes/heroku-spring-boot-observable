package com.company.myapp.service;

import java.lang.reflect.Field;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.company.myapp.config.ReaderProperties;
import com.company.myapp.model.Channel;
import com.company.myapp.model.Feed;
import com.company.myapp.model.Reading;

@Component
public class ThingspeakFeedReader implements FeedReader {
	private static final Logger logger = Logger.getLogger(ThingspeakFeedReader.class);
	
	private static final String JSON_EXTENSION = ".json";
	private static final Long LAST = 1L;
	private static final Long NO_RECORDS = 0L;
	private RestTemplate restTemplate = new RestTemplate();
	private static final int ID_LENGHT = 6;

	private String feedReaderId = UUIDGenerator.nextUUID(ID_LENGHT);
	
	public String channelRoot;
	public String channelId;
	public String apiKey;
	public String records;
	
	private Feed feed;
	
	private String baseUrl;
	private String lastFeedUrl;
	
	@Autowired
	public ThingspeakFeedReader(ReaderProperties readerProperties) {
		loadProperties(readerProperties);
		this.baseUrl = channelRoot + "/" + channelId + "/feeds";
		this.lastFeedUrl = buildFeedUrl(LAST);
	}
	
	public Feed readLastFeed() {
		logger.info("      Feed Reader(" + feedReaderId + ") reading last feed...");
		feed = restTemplate.getForObject(lastFeedUrl, Feed.class);
		feed.setChannelId(channelId);
		logger.info("      Feed Reader(" + feedReaderId + ")             reading: " + feed);
		return feed;
	}

	public Reading readRecords() {
		logger.info("      Feed Reader(" + feedReaderId + ") reading records...");
		logger.debug("Reading records...");
		Long recordsNumber = Long.valueOf(records);
		String url = buildRecordsUrl(recordsNumber);
		Reading reading = restTemplate.getForObject(url, Reading.class);
		return reading;
	}
	
	public Channel getChannel() {
		logger.info("      Feed Reader(" + feedReaderId + ") reading channel...");
		String url = buildRecordsUrl(NO_RECORDS);
		Reading reading = restTemplate.getForObject(url, Reading.class);
		Channel channel = reading.getChannel();
		return channel;
	}

	public Feed readFeed(Long entryId) {
		logger.info("      Feed Reader(" + feedReaderId + ") reading feed, entryId: " + entryId + "...");
		String url = buildFeedUrl(entryId);
		feed = restTemplate.getForObject(url, Feed.class);
		feed.setChannelId(channelId);
		return feed;
	}
	
	private String buildRecordsUrl(Long count) {
		logger.info("      Feed Reader(" + feedReaderId + ") building records url, for " + count + " records...");
		String countStr = String.valueOf(count);
		String url = "";

		if (!apiKey.isEmpty()) {
			if (count >= 0) {
				url = baseUrl + JSON_EXTENSION + "?" + "results=" + countStr + "&" + "&api_key=" + apiKey;
			} else {
				url = baseUrl + JSON_EXTENSION + "?" + "&api_key=" + apiKey;
			}
		} else {
			if (count >= 0) {
				url = baseUrl + JSON_EXTENSION + "?" + "results=" + countStr;
			} else {
				url = baseUrl + JSON_EXTENSION;
			}
		}

		return url;
	}

	private String buildFeedUrl(Long entryId) {
		logger.debug("Building feed url...");
		String url = "";
		String entry = "";

		if (entryId > 1) {
			entry = "/" + String.valueOf(entryId);
		} else {
			entry = "/last";
		}

		if (!apiKey.isEmpty()) {
			url = baseUrl + entry + JSON_EXTENSION + "?" + "&api_key=" + apiKey;
		} else {
			url = baseUrl + entry + JSON_EXTENSION;
		}

		return url;
	}
	
	//TODO add check for required fields
	private void loadProperties(ReaderProperties readerProperties) {
		Map<String, String> readerPropertiesMap = readerProperties.getPropertiesMap();
		Field[] fields = this.getClass().getFields();
		for (Field field : fields) {
			String fieldName = field.getName();
			String fieldValue = readerPropertiesMap.get(fieldName);
			logger.debug("fieldName/fieldValue: " + fieldName + ": " + fieldValue);
			
			if(fieldValue != null){
				try {
					field.set(this, fieldValue);
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
					logger.error("Unable to set reader property..." + e);
				} catch (IllegalAccessException e) {
					e.printStackTrace();
					logger.error("Unable to set reader property..." + e);
				}
			}
		}
	}
}
