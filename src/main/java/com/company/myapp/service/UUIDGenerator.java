package com.company.myapp.service;

import java.util.UUID;

import org.springframework.stereotype.Component;

@Component
public class UUIDGenerator {
	public static String nextUUID() {
		return UUID.randomUUID().toString();
	}
	public static String nextUUID(int chars) {
		String uuid = UUID.randomUUID().toString();
		int uuidLen = uuid.length();
		if(chars >= 6 && chars <= uuidLen){
			return uuid.substring(uuidLen - chars, uuidLen);
		} else {
			return uuid;
		}
	}
}
