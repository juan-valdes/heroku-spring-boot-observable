package com.company.myapp.service;

import com.company.myapp.model.Feed;

public interface FeedReader {
	public Feed readLastFeed();
}
