package com.company.myapp.service;

import com.company.myapp.model.Feed;

public interface FeedDisplay {
	public void displayFeed(Feed feed);
}
