package com.company.myapp;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.company.myapp.service.Bus;
import com.company.myapp.service.BusImpl;
import com.company.myapp.service.Subject;
import com.company.myapp.service.SubjectImpl;

@SpringBootApplication
public class Application {
	private static final Logger logger = Logger.getLogger(Application.class);

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(Application.class, args);
		
		ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(5);
		
		Runnable feedReaderRunner = ctx.getBean("feedReaderRunner", Runnable.class);
		scheduledExecutorService.scheduleAtFixedRate(() -> {
			feedReaderRunner.run();
		}, 0L, 19L, TimeUnit.SECONDS);
		
		Runnable feedNotifierRunner = ctx.getBean("feedNotifierRunner",Runnable.class);
		scheduledExecutorService.scheduleAtFixedRate(() -> {
			feedNotifierRunner.run();
		}, 10L, 19L, TimeUnit.SECONDS);
		
	}
}
