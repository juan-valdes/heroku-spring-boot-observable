package com.company.myapp.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Channel {

	private Long id;
	private String name;
	private String description;
	private Float latitude;
	private Float longitude;

	private String field1;
	private String field2;

	@JsonProperty("created_at")
	private Date createdAt;
	@JsonProperty("updated_at")
	private Date updatedAt;

	@JsonProperty("last_entry_id")
	private Long lastEntryId;

	public Channel() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	public String getField1() {
		return field1;
	}

	public void setField1(String field1) {
		this.field1 = field1;
	}

	public String getField2() {
		return field2;
	}

	public void setField2(String field2) {
		this.field2 = field2;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Long getLast_entry_id() {
		return lastEntryId;
	}

	public void setLast_entry_id(Long last_entry_id) {
		this.lastEntryId = last_entry_id;
	}

	@Override
	public String toString() {
		return "Channel {" + "id: " + id 
				+ ", name: " + name 
				+ ", description: " + description 
				+ ", latitude: " + latitude 
				+ ", longitude: " + longitude 
				+ ", field1: " + field1 
				+ ", field2: " + field2 
				+ ", createdAt: " + createdAt 
				+ ", updatedAt: " + updatedAt
				+ ", lastEntryId: " + lastEntryId + " }";
	}
}
