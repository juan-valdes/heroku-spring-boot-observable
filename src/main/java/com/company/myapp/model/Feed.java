package com.company.myapp.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Feed {
	
	private String channelId;
	
	@JsonProperty("entry_id")
	private Long entryId;
	@JsonProperty("created_at")
	private Date createdAt;
	private float field1;
	private float field2;//TODO add more fields
	
	public Feed(){
	}
	
	public String getChannelId() {
		return channelId;
	}
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	
	public Long getEntryId() {
		return entryId;
	}
	public void setEntryid(Long entryid) {
		this.entryId = entryid;
	}
	
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
	public float getField1() {
		return field1;
	}
	public void setField1(float field1) {
		this.field1 = field1;
	}
	
	public float getField2() {
		return field2;
	}
	public void setField2(float field2) {
		this.field2 = field2;
	}
	
	@Override
    public String toString() {
        return "Feed {" 
        		+ "channelId: " + channelId 
        		+ ", entryId: " + entryId 
                + ", createdAt: " + createdAt 
                + ", field1: " + field1 
                + ", field2: " + field2 
                + "}";
    }
}
