package com.company.myapp.model;

import java.util.ArrayList;
import java.util.List;

import com.company.myapp.service.Bus;

public class FeedObservers {

	private Feed feed;
	private List<Bus> observerList;
	
	public Feed getFeed() {
		return feed;
	}

	public void setFeed(Feed feed) {
		this.feed = feed;
	}

	public List<Bus> getObserverList() {
		return observerList;
	}
	
	public List<String> getObserverIdList() {
		List<String> observerIdList = new ArrayList<String>();
		for (Bus observer : observerList) {
			observerIdList.add(observer.getId());
		}
		return observerIdList;
	}

	public void setObserverList(List<Bus> observerList) {
		this.observerList = observerList;
	}
	
	public void setFeedAndObserverList(Feed feed, List<Bus> observerList) {
		this.feed = feed;
		this.observerList = observerList;
	}
	
	@Override
	public String toString(){
		String str = "feedObservers {"
				+ feed
				+ ", observers: " + getObserverIdList()
				;
		return str;
	}

}
