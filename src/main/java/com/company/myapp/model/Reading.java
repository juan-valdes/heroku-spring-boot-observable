package com.company.myapp.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Reading {
	
	private Channel channel;
	private List<Feed> feeds;
	
	public Reading(){
	}
	
	public Channel getChannel() {
		return channel;
	}
	public void setChannel(Channel channel) {
		this.channel = channel;
	}
	public List<Feed> getFeeds() {
		return feeds;
	}
	public void setFeeds(List<Feed> feeds) {
		this.feeds = feeds;
	}
	
	@Override
    public String toString() {
        return "Reading {" +
                channel.toString() + ", " +
                feeds.toString() + "}";
	}

}
